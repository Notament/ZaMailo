availableMail = [];

$( ".auto" ).keypress(function() {
    if(availableMail.length === 0){
    fetch('./contact/getAll')
        .then(function(response) {
            return response.json();
        })
        .then(function(json) {
                json.forEach(function(element) {
                    availableMail.push(element);
                })
            AutoComplete(availableMail);
        })
    }
    else{
        AutoComplete(availableMail);
    }
  });

  function AutoComplete(availableMail){
    $( ".auto" ).autocomplete({
        source: availableMail,
        appendTo: ".autocompletion_item",
        messages: {
            noResults: '',
            results: function() {}
        }
      });
  }
