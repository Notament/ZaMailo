<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use App\Entity\Email;
use App\Entity\Category;
use App\Form\EmailType;
use App\Entity\Contact;
use App\Form\CategoryType;
use App\Form\ContactType;
use \Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/mail")
 */
class MainController extends Controller
{
    /**
     * @Route("/inbox", name="main")
     */
    public function inbox(Request $request)
    {
        $mails = $this->getDoctrine()
        ->getRepository(Email::class)
        ->findAll();

        $categorys = $this->getAllCategory();

        $defaultData = array('message' => 'Type your message here');

        $search_bar = $this->searchBarForm($defaultData);

        $search_bar->handleRequest($request);

        if ($search_bar->isSubmitted() && $search_bar->isValid()) {

            $search_value = htmlspecialchars($search_bar->getData()['search']);
            $filtre = htmlspecialchars($search_bar->getData()['Trier']);
            $search_value_string = 'Searching for "'. $search_value .'"';

            $mails = $this->getDoctrine()
            ->getRepository(Email::class)
            ->findByFilter($search_value,$filtre);

            return $this->render('main/category.html.twig', [
                'mails' => $mails,
                'category' => $search_value_string,
                'categorys' => $categorys
            ]);

        }


        return $this->render('main/index.html.twig', [
            'mails' => $mails,
            'search_bar' => $search_bar->createView(),
            'categorys' => $categorys
        ]);
    }

    /**
     * @Route("/inbox/{id}", name="byId", requirements={"id":"\d+"})
     */
    public function byId($id, Request $request)
    {
        $em = $this->getDoctrine()
                   ->getManager();
        $mail = $em->getRepository(Email::class)
                   ->find($id);
        $categorys = $this->getAllCategory();

        $array_categories;
        foreach($categorys as $category){
            $array_categories[$category->getName()] = $category->getName();
        }

        //Form Lié à aucune entité mais nécéssaire pour sélectionné les différentes catégories
       $defaultData = array('message' => 'Type your message here');
       $form = $this->createFormBuilder($defaultData)
                    ->add('Categorie', ChoiceType::class, array(
                            'label' => '',
                            'choices'  => $array_categories
                    ))
                    ->add('save', SubmitType::class, array('label' => 'Ajouter une catégorie'))
                    ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //Séléction du champ
            $data = $form->getData()["Categorie"];
            //Récupération via une requête SQL D'une catégorie par rapport au champ récupéré
            $category = $this->getDoctrine()
                        ->getManager()
                        ->getRepository(Category::class)
                        ->findByCategoryName($data);
            //Assignation à l'email en question de la catégorie dont il dépend via une Instance de Catégorie
            $mail->setCategory($category);
            $em->flush();

            return $this->redirectToRoute('main');
        }


        return $this->render('main/mail.html.twig', [
            'mail' => $mail,
            'form' => $form->createView(),
            'categorys' => $categorys
        ]);
    }


    /**
     * @Route("/new", name="newMail")
     */
    public function new(Request $request){
        $contacts = $this->getDoctrine()
        ->getRepository(Contact::class)
        ->findAll();

        $email = new Email();

        $form = $this->createForm(EmailType::class, $email)
            ->add('save', SubmitType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $email = $form->getData();
                foreach($contacts as $contact){
                    if($contact->getEmail() === $email->getRecipient()){
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($email);
                        $em->flush();

                        return $this->redirectToRoute('main');
                    }
                    else{
                        $error = "L'adresse que vous avez validée ne fait pas parti de votre agenda de contact, veuillez lier un contact à cette addresse mail pour l'utiliser";
                    }
                }
        }

        $categorys = $this->getAllCategory();

        return $this->render('main/form.html.twig', [
            'form' => $form->createView(),
            'categorys' => $categorys,
            'error' => $error = isset($error) ? $error: ''
        ]);
    }

    /**
     * @Route("/category/new", name="newCategory")
     */
    public function newCategory(Request $request){

        $category = new Category();

        $form = $this->createForm(CategoryType::class, $category)
            ->add('save', SubmitType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $category = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($category);
                $em->flush();

                return $this->redirectToRoute('main');

        }

        $categorys = $this->getAllCategory();

        return $this->render('main/form_cat.html.twig', [
            'form' => $form->createView(),
            'categorys' => $categorys
        ]);
    }

    /**
     * @Route("/category/{param}", name="byCategory")
     */
    public function filterByCategory($param){
        $mails = $this->getDoctrine()
        ->getRepository(Email::class)
        ->byCategory($param);

        $categorys = $this->getAllCategory();

        return $this->render('main/category.html.twig', [
            'mails' => $mails,
            'categorys' => $categorys,
            'category' => $param
        ]);
    }

    /**
     * @Route("/contact/new", name="newContact")
     */
    public function newContact(Request $request){

        $contact = new Contact();

        $form = $this->createForm(ContactType::class, $contact)
            ->add('save', SubmitType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contact = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($contact);
                $em->flush();

                return $this->redirectToRoute('contact');

        }

        $categorys = $this->getAllCategory();

        return $this->render('main/form_contact.html.twig', [
            'form' => $form->createView(),
            'categorys' => $categorys
        ]);
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function AllContact(){
        $contacts = $this->getDoctrine()
        ->getRepository(Contact::class)
        ->findAll();

        $categorys = $this->getAllCategory();


        return $this->render('main/contact.html.twig', [
            'contacts' => $contacts,
            'categorys' => $categorys
        ]);
    }

    /**
     * @Route("/contact/edit/{id}", name="editContact")
     */
    public function editContact($id, Request $request){
        $em = $this->getDoctrine()->getManager();
        $contact = $em->getRepository(Contact::class)->find($id);

        $form = $this->createForm(ContactType::class, $contact)
        ->add('save', SubmitType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();

            return $this->redirectToRoute('contact');
        }

        $categorys = $this->getAllCategory();

        return $this->render('main/form_contact.html.twig', array(
            'form' => $form->createView(),
            'categorys' => $categorys
        ));

    }

    /**
     * @Route("/contact/delete/{id}", name="deleteContact")
     */
    public function deleteContact($id){
        $em = $this->getDoctrine()->getManager();
        $contact = $em->getRepository(Contact::class)->find($id);

        $em->remove($contact);
        $em->flush();

        return $this->redirectToRoute('contact');
    }

    //REQUETE AJAX POUR RECUPERER LE TABLEAU DE CONTACT POUR LA CONCATÉNATION
    /**
     * @Route("/contact/getAll", name="GetAll")
     */
    public function getAll(){
        $contacts = $this->getDoctrine()
        ->getRepository(Contact::class)
        ->findAll();
        $datas;
        foreach($contacts as $contact){
            $datas[] = $contact->getEmail();
        }
        return $response = new JsonResponse($datas);
    }

    public function getAllCategory(){
        return $this->getDoctrine()
        ->getRepository(Category::class)
        ->findAll();
    }

    public function searchBarForm($defaultData){

        return $this->createFormBuilder($defaultData)
                 ->add('search', TextType::class)
                 ->add('Trier', ChoiceType::class, array(
                         'label' => '',
                         'choices'  => array(
                             'Par sujet' => 'subject',
                             'Par Message' => 'message'
                        )
                    ))
                 ->add('save', SubmitType::class, array('label' => 'Chercher un mail avec ces paramètres'))
                 ->getForm();
    }

}
